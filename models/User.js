const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        maxLength: 100,
        required: true
    },
    username: {
        type: String,
        unique: true,
        trim: true,
        maxLength: 50,
        required: true
    },
    phoneNumber: {
        type: String,
        trim: true,
        maxLength: 20
    },
    profile: {
        firstName: String,
        lastName: String,
        age: {
            type: Number,
            min: 9,
            max: 100,
            maxLength: 3
        }
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
}, {
    collection: 'users'
});

const User = model('User', userSchema);
module.exports = User;