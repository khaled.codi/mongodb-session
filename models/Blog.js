const { Schema, model } = require('mongoose');

const blogSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    content: String,
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    collection: 'blogs'
});

const Blog = model('Blog', blogSchema);
module.exports = Blog;